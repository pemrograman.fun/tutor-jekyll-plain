---
layout      : post
title       : Company of Thieves - Oscar Wilde
date        : 2020-03-15 07:35:05 +0700
categories  : lyric
tags        : [indie, 2010s]
author      : Company of Thieves

opengraph:
  image: https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2F91d2472664a910cdfafbe3749f2767d9.600x600x1.jpg
---

Time keeps on ticking away  
It's Always running away  
We're always running in time  
We're always running from time

We are all our own devil  
We are all our own devil  
And we make this world our hell
