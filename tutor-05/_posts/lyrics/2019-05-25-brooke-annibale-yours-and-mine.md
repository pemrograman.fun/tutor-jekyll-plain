---
layout      : post
title       : Brooke Annibale - Yours and Mine
date        : 2019-05-25 07:35:05 +0700
categories  : lyric
tags        : [pop, 2010s]
keywords    : [OST, "One Tree Hill"]
author      : Brooke Annibale

excerpt     : >-
  Take yours and mine, we'll combine.
  And we'll have everything.

opengraph:
  image: https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2Ff087f08ff97ece407cbd46ccb8ef5405.1000x1000x1.jpg
---

All we've got is all we have to give away  
So what I have not I fill in with what you got  
And we'll be okay

Take yours and mine, we'll combine  
And we'll have everything

Take yours and mine, we'll combine  
And we'll have everything
