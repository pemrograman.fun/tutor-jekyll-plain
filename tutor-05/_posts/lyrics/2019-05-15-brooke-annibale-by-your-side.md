---
layout      : post
title       : Brooke Annibale - By Your Side
date        : 2019-05-15 07:35:05 +0700
categories  : lyric
tags        : [pop, 2010s]
keywords    : [OST, "Grey's Anatomy"]
author      : Brooke Annibale

excerpt     :
  No more airplanes.
  No more long goodbyes.

opengraph:
  image: https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2Fb60b95ddf3c9116dc25191eaa93f8931.1000x1000x1.jpg
---


Someday some day soon

No more airplanes  
No more long goodbyes  
'Cause I'm staying right by your side
