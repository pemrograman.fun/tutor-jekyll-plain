---
layout      : post
title       : Emma Ruth Rundle - Shadows of My Name
date        : 2018-02-15 07:35:05 +0700
categories  : lyric
tags        : [rock, 2010s]
author      : Emma Ruth Rundle

opengraph:
  image: https://t2.genius.com/unsafe/220x220/https%3A%2F%2Fimages.genius.com%2Fb316903093dd4404f238bd684b38f5b2.350x350x1.jpg
---

I lay back in salt  
Please forgive my name  
I won’t speak at all  
Just to sing again
