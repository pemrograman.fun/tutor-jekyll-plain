### Tutor 05

> Pagination with Liquid

* Pagination (v1): Simple, Number, Adjacent, Indicator

* Pagination (v2): Simple, Number, Adjacent, Indicator

* Meta HTML

* Post Navigation

![Jekyll Plain: Tutor 05][jekyll-plain-preview-05]

-- -- --

What do you think ?

[jekyll-plain-preview-05]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-05/jekyll-plain-preview.png
