### Tutor 02

> Include Liquid Partials

* Setup Directory for Minimal Jekyll

* New Layout: [Page, Post, Home] (extends Default)

* Partials: HTML Head, Header, Footer

* Basic Content

![Jekyll Bulma: Tutor 02][jekyll-plain-preview-02]

-- -- --

What do you think ?

[jekyll-plain-preview-02]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-02/jekyll-plain-preview.png
