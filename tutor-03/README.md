### Tutor 03

> Loop with Liquid

* More Content: Lyrics. This is required for loop demo

* Index Content: Index, Category, Tag, Archive By Year/Month

![Jekyll Plain: Tutor 03][jekyll-plain-preview-03]

-- -- --

What do you think ?

[jekyll-plain-preview-03]:  https://gitlab.com/epsi-rns/tutor-jekyll-plain/raw/master/tutor-03/jekyll-plain-preview.png
